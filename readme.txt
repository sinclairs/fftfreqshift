
freqscale.py
------------
Stephen Sinclair MUMT Fall 2005
radarsat1@gmail.com


This archive contains a Python program implementing Laroche & Dolson's 1999
technique for performing arbitrary shifting operations in the frequency
domain.  It implements a non-realtime frequency scaling where the ratio
may be specified as any real value > 0.  (Frequencies scaled beyond the FFT
range, however, are truncated rather than mirrored back into the negative
frequencies.)

It operates on any .wav file in 16-bit Mono format, though the file must
fit in memory.  The command line usage is as follows:

freqscale.py <ratio> <fftsize> <filename> [plot] [aligned]

The output will be written to a new wave file called "out.wav".  If the [plot]
argument is given, FFT pre- and post-shifted peaks will be plotted, as well
as the output after each frame.  This is for testing and debugging purposes.
If the [aligned] argument is given, frequency shifts will be bin-aligned and
not use any interpolation techniques.  This is interesting for comparison
purposes.

You may use the included "gensin.py" program to generate any combination
of sine waves for testing.

Its command line usage is as follows:

./gensin.py <filename> <seconds> <hz1> [<hz2> ...]


This program requires the pylab libraries, downloadable from
http://matplotlib.sourceforge.net

And of course, it also requires Python, available at:
http://python.org

Please send me any feedback, comments, or suggestions you may have.


Known Problems:
- Signals with noise, as well as voice signals, may result in
  some phasiness distortion.
- Low frequencies may be distorted by the linear interpolation.  This is
  under investigation.

Anyone is permitted to use , modify ad distribute this code under the
BSD-3-Clause license as mentioned in the file LICENSE found in this
directory.
