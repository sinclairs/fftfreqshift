#!/usr/bin/env python2

import wave
import sys
import math

try:
	from pylab import *
	#from FFT import fft, inverse_fft
	from numpy.fft import fft, ifft
except:
	print 'freqscale.py requires FFT and pylab modules.'
	sys.exit()

import array

#ifft = inverse_fft

filename = 'sin.wav'
fftsize = 1024
SR = 44100
ratio = 2
overlap = 4
doplot = False
aligned = False

if len(sys.argv) < 2:
	print "Usage: freqscale.py <ratio> <fftsize> <filename> [plot] [aligned]"
	print "Output will be written to 'out.wav'."
	sys.exit()

if len(sys.argv) > 1:
	ratio = float(sys.argv[1])
	if (ratio==0):
		print "You probably don't want a ratio of 0."

if len(sys.argv) > 2:
	fftsize = int(sys.argv[2])

if len(sys.argv) > 3:
	filename = sys.argv[3]

for i in sys.argv[4:]:
	if i=='plot':
		doplot = True
	if i=='aligned':
		aligned = True

print "File:", filename, "\tSize:", fftsize, "\tRatio: ", ratio

def readwave(name):
	global fftsize
	f = wave.open(name, 'r')
	b = f.readframes(f.getnframes())
	print "Input: ", f.getnframes()
	f.close()
	return fromstring(b, np.int16) / 32767.0

def writewave(name, a):
	f = wave.open(name, 'w')
	f.setframerate(SR)
	f.setnchannels(1)
	f.setsampwidth(2)

	n=0
	while (n<len(a)):
		b = ''
		for i in range(1024):
			if n<len(a):
				j = min(max(int(a[n]*32768),-32767),32767)
				b += chr(j & 0xFF)
				b += chr((j >> 8) & 0xFF)
				n += 1
		f.writeframes(b)
	print "Output: ", n
	f.close()

def phasevocoder(a, function, fftsize=1024, window=hanning(1024)):
	# prepare output array
	output = a.copy()
	for n in range(len(output)):
		output[n] = 0

	b = a[:fftsize].copy()
	d = [0+0j] * len(b)
	for i in range(len(a)/fftsize*overlap):
		sys.stdout.write("%02d%%  %d\r" % ((i*100/(len(a)/fftsize*overlap), i*fftsize/overlap)))
		sys.stdout.flush()
		# apply the window
		for n in range(len(b)):
			if i*fftsize/overlap+n < len(a):
				b[n] = window[n] * a[i*fftsize/overlap+n]
			else:
				b[n] = 0

		# perform FFT
		c = fft(b)

		# frequency-space transformation
		function(c, d)

		# perform IFFT and add real values to output
		e = ifft(d)
		for n in range(len(e)):
			if i*fftsize/overlap+n < len(output):
				output[i*fftsize/overlap+n] += e[n].real
		if doplot:
			show(plot(output[0:i*fftsize/overlap+n]))
	print "Done.            "

	return output


# return a linear interpolation
# specified by a fraction array position
def interpolate(array, pos):
	offset = pos-int(pos)
	pos = int(pos)
	return array[pos]*(1.0-offset) + array[pos+1]*(offset)

# return the x position of the parabolic peak
# specified by the 3 closest points.
def parabolic(array, pos):
	return (pos -
		 (array[pos-1]-array[pos+1])
		 / (2*(2*array[pos]-array[pos-1]-array[pos+1])))

z = [0]*fftsize
def freqscale(input, output):
	c = [0+0j] * len(output)
	peaks = []
	absinput = abs(input)
	
	# pick peaks
	for n in range(len(input)/2):
		if n==0 or n==(len(input)-1):
			continue
		if absinput[n] > absinput[n-1] and absinput[n] > absinput[n+1]:
			p1 = n-1
			while p1 >= 0 and absinput[p1] < absinput[p1+1]:
				p1 -= 1
			p2 = n+1
			while p2 < len(input) and absinput[p2-1] > absinput[p2]:
				p2 += 1
			if aligned:
				peaks.append((n, p1+1, p2-1))
			else:
				peaks.append((parabolic(absinput, n), p1+1, p2-1))

		# zero the output spectrum
		output[n] = 0

	# shift each peak
	for pk in peaks:
		f = pk[0]
		bins = f*ratio - f
		if aligned:
			bins = int(bins)
		if (f+bins >= fftsize):
			continue
		f = int(f)

		deltawR = bins*math.pi*2/fftsize * fftsize/overlap
		z[f] += deltawR

		for i in range(int(pk[1]+bins),int(pk[2]+bins)):
			if (i < fftsize) and (i >= 0):
				c[i] = input[int(i-bins)] * complex(cos(z[f]),sin(z[f]))
				if aligned:
					output[i] = input[int(i-bins)] * complex(cos(z[f]),sin(z[f]))
				else:
					output[i] = interpolate(input, i-bins) * complex(cos(z[f]),sin(z[f]))

	# copy negative spectrum
	for n in range(fftsize/2,fftsize):
		output[n] = output[n-fftsize]
		
	# plotting
	if doplot:
		x = [0] * len(output)
		for n in range(len(x)):
			x[n] = abs(input[n])/fftsize
		plot(x[:75])
#		plot(x)[:50], 'o')
		for n in range(len(x)):
			x[n] = abs(c[n])/fftsize
		plot(x[:75])
#		plot(x[:50], 'gs')
		for n in range(len(x)):
			x[n] = abs(output[n])/fftsize
		plot(x[:75])
#		plot(x[:50], 'r^')
		show()
	
# read input wave and perform FFT
a = readwave(filename)

# prepare windowing function
window = hanning(fftsize)

# perform phase vocoding
output = phasevocoder(a, freqscale, fftsize, window)

# write output
writewave('out.wav', output)
