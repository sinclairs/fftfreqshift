#!/usr/bin/env python

import math
import wave
import numpy
import array
import sys

SR = 44100

def writewave(name, a):
	f = wave.open(name, 'w')
	f.setframerate(SR)
	f.setnchannels(1)
	f.setsampwidth(2)

	b = array.array('h')
	b.fromlist([0]*1024)
	n=0
	while (n<len(a)):
		for i in range(len(b)):
			if n<len(a):
				b[i] = min(max(int(a[n]*32768),-32767),32768)
				n += 1
		f.writeframes(b)
	f.close()

def main():
	if len(sys.argv) < 4:
		print './gensin.py <filename> <seconds> <hz1> [<hz2> ...]'
		return

	filename = sys.argv[1]
	length = int(float(sys.argv[2])*SR)
	hz = [0]*(len(sys.argv)-3)
	for n in range(len(hz)):
		hz[n] = int(sys.argv[n+3])
		
		a = numpy.array([0.0]*length)
		for n in range(len(a)):
			a[n] = 0
			for i in range(len(hz)):
				a[n] += math.sin(2*numpy.pi*hz[i]*(n*1.0/SR))/len(hz)

	writewave(filename, a)

if __name__=='__main__':
	main()
